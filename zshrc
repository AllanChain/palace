export ZSH="$HOME/.oh-my-zsh"
if [[ -d $ZSH ]]; then
  ZSH_THEME="powerlevel10k/powerlevel10k"
  DISABLE_AUTO_UPDATE="true"
  plugins=(git zsh-syntax-highlighting)
  source $ZSH/oh-my-zsh.sh
fi

if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

if [[ -x /usr/bin/pyenv ]]; then
  eval "$(pyenv init -)"
  export PYENV_ROOT="$HOME/.pyenv"
  export PATH=$PYENV_ROOT/bin:$PATH
fi
if [[ -x /usr/bin/thefuck ]]; then
  eval "$(thefuck --alias)"
fi
if [[ -x /usr/bin/zoxide ]]; then
  eval "$(zoxide init zsh)"
fi
if [[ -x /usr/bin/direnv ]]; then
  eval "$(direnv hook zsh)"
fi
if [[ -x /usr/bin/rtx ]]; then
  eval "$(rtx activate zsh)"
fi
if [[ -x /usr/bin/atuin ]]; then
  eval "$(atuin init zsh --disable-up-arrow)"
fi

if [[ -x /usr/bin/trash ]]; then
  alias rm='echo "!!! THIS IS RM !!!"; false'
  alias rmt=trash
fi
if [[ -x /usr/bin/bat ]]; then
  alias bat='bat -p'
fi
if [[ -x /usr/bin/eza ]]; then
  alias l='eza -albgh --icons=always'
  alias ll='eza -lbgh --icons=always'
  alias ls=eza
  alias la='ls -a'
fi
if [[ -d $HOME/go/bin ]]; then
  export PATH="$HOME/go/bin:$PATH"
fi

function gi() { curl -sLw "\n" https://www.toptal.com/developers/gitignore/api/$@ ;}

if [[ -z $(ssh-add -L) ]]; then
  ssh-add -q ~/.ssh/id_ed25519 < /dev/null
fi
