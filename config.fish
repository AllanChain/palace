if status is-interactive
    if test -x /usr/bin/zoxide
        zoxide init fish | source
    end
    if test -x /usr/bin/direnv
        direnv hook fish | source
    end
    if test -x /usr/bin/pyenv
        pyenv init - | source
    end
    if test -x /usr/bin/trash
        abbr -a rm trash
    end
    if test -x /usr/bin/eza
        abbr -a l 'eza -albgh --icons=always'
        abbr -a ll 'eza -lbgh --icons=always'
        abbr -a ls eza
        abbr -a la 'ls -a'
    end
    if test -x /usr/bin/neovide
        abbr -a nvid neovide
    end
    if test -x /usr/bin/git
        source (dirname (status -f))/git-cmd.fish
    end
    if test -f ~/.asdf/asdf.fish
        source ~/.asdf/asdf.fish
    end
    function ...
        ../..
    end
    function ....
        ../../..
    end
    # Commands to run in interactive sessions can go here
end
