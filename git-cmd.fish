abbr -a ga git add
abbr -a gb git branch
abbr -a gco git checkout
abbr -a gcb git checkout -b
abbr -a gc git commit
abbr -a gca git commit --all
abbr -a gd git diff
abbr -a gf git fetch
abbr -a gm git merge
abbr -a gl git pull
abbr -a gp git push
abbr -a grst git restore --staged
abbr -a gst git status
abbr -a gcm 'git checkout (git_main_branch)'
abbr -a gcd 'git checkout (git_develop_branch)'
alias glol='git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset"'
function git_main_branch
    git rev-parse --git-dir &>/dev/null; or return
    for ref in refs/{heads,remotes/{origin,upstream}}/{main,trunk,mainline,default,master}
        if git show-ref -q --verify $ref
            basename $ref
            return 0
        end
    end
    echo master
    return 1
end
function git_develop_branch
    git rev-parse --git-dir &>/dev/null; or return
    for branch in dev devel develop development
        git show-ref -q --verify refs/heads/$branch
        echo $branch
        return 0
    end
    echo develop
    return 1
end
