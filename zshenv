# https://wiki.archlinux.org/title/Fcitx5
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

export SSH_ASKPASS_REQUIRE=force
export SSH_ASKPASS=/usr/bin/ksshaskpass

export EDITOR='/usr/bin/nvim'
export TERMINAL=konsole

export GO111MODULE=on
export GOPROXY=https://goproxy.cn
export JULIA_PKG_SERVER=https://mirrors.tuna.tsinghua.edu.cn/julia
export RUSTC_WRAPPER=sccache

export MOZ_USE_XINPUT2=1
export MOZ_ENABLE_WAYLAND=1
export GTK_USE_PORTAL=1

export TEXMFCNF=$HOME/texmf:
export TLDR_PAGES_SOURCE_LOCATION="https://mirror.ghproxy.com/raw.githubusercontent.com/tldr-pages/tldr/main/pages"
